﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW9_Multithread
{
    interface IArrayCalculator
    {
        int GetSum(int[] array);
    }
}
