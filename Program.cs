﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
namespace HW9_Multithread
{
    class Program
    {
        static void Main(string[] args)
        {
            var testList = new List<int>() { 100000, 1000000, 10000000 };
            var simpleSummator = new SimpleSummator();
            var linqSummator = new LinqSummator();
            var multiThreadSummator = new MultiThreadSummator(Environment.ProcessorCount);
            int sum = 0;
            foreach (int test in testList)
            {
                var arr = new int[test];
                for (int i = 0; i < test; i++)
                    arr[i] = 1;
                                
                var sw = new Stopwatch();
                sw.Start();
                sum = simpleSummator.GetSum(arr);
                sw.Stop();
                Console.WriteLine($"Simple mode. Sum = {sum} duration = {sw.ElapsedMilliseconds}");
                sw.Reset();
                sw.Start();
                sum = multiThreadSummator.GetSum(arr);
                sw.Stop();
                Console.WriteLine($"MultiThread mode. Sum = {sum} duration = {sw.ElapsedMilliseconds}");
                sw.Reset();
                sw.Start();
                sum = linqSummator.GetSum(arr);
                sw.Stop();
                Console.WriteLine($"LINQ mode. Sum = {sum} duration = {sw.ElapsedMilliseconds}");
            }
            
        }
    }
}
